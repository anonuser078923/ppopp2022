# Implementations
The implementations are found in the `ds` folder.

# To build all data structures
```
cd microbench
make -j skip_validation=1
```
Binaries appear in `microbench/bin`.

# To run microbenchmark
From setbench/microbench folder (change thread counts as needed):
```
make -j skip_validation=1; rm logfile.txt; echo "ZIPF INS_DEL ALG KEYS THREADS THROUGHPUT" > comparison.txt; for alg in bin/occ_abtree.debra bin/elim_abtree.debra bin/aksenov_splaylist_64.debra bin/brown_ext_abtree_lf.debra bin/brown_ext_ist_lf.debra bin/bronson_pext_bst_occ.debra bin/guerraoui_ext_bst_ticket.debra bin/morrison_cbtree.debra bin/natarajan_ext_bst_lf.debra bin/OLC_ART.debra bin/wang_openbwtree.debra bin/winblad_catree.debra; do for zipf in 0 1.00000001; do for insdel in "2.5 2.5" "10 10" "25 25" "50 50"; do for keys in 20000 200000 2000000 20000000; do for tcount in 18 36 72 108 144; do for rep in 0 1 2; do (echo -n "$zipf $insdel $alg $keys $tcount "; LD_PRELOAD=../lib/libjemalloc.so numactl -i all $alg -nwork $tcount -nprefill $tcount -prefill-insert -insdel $insdel -k $keys -t 10000 -pin $(experiments/get_pinning_cluster.sh) -dist-zipf $zipf 2>&1 | tee -a "logfile.txt" | grep "total throughput" | cut -d: -f2) | column -t; done; done; done; done; done; done >> comparison.txt
```

This replicates our throughput numbers. It loops through each experiment and runs it 3 times.
The resulting throughput values can be found in `comparison.txt`.

# To run YCSB
Uncomment the setbench_error in erase in ds/wang_openbwtree/adapter.h. In macrobench_experiments/abtree_exp, alter any values you want in `_user_experiment.py`, then run `run_production.sh`.
