/*
 * File:   keygen.h
 * Author: t35brown
 *
 * Created on August 5, 2019, 7:24 PM
 */

#ifndef KEYGEN_H
#define KEYGEN_H

#include <algorithm>
#include <iostream>
#include <cassert>
#include "plaf.h"

using namespace std;

template <typename K>
class KeyGeneratorUniform {
private:
    PAD;
    RandomFNV1A * rng;
    int maxKey;
    PAD;
public:
    KeyGeneratorUniform(RandomFNV1A * _rng, int _maxKey) : rng(_rng), maxKey(_maxKey) {}
    K next() {
        auto result = 1+rng->next(maxKey);
        assert((result >= 1) && (result <= maxKey));
        return result;
    }
};




/* 
===========================================================================================
The KeyGeneratorZipf class can be used to generate arbitrary Zipfian distribution, even
exponent 0 (uniform). The ZipfRejectionInversionSampler can only be used with exponent > 1.
===========================================================================================
*/

// class KeyGeneratorZipfData {
// public:
//     PAD;
//     int maxKey;
//     double c = 0; // Normalization constant
//     double* sum_probs; // Pre-calculated sum of probabilities
//     PAD;

//     KeyGeneratorZipfData(const int _maxKey, const double _alpha) {
//         maxKey = _maxKey;

//         // Compute normalization constant c for implied key range: [1, maxKey]
//         for (int i = 1; i <= _maxKey; i++) {
//             c += ((double)1) / pow((double) i, _alpha);
//         }

//         double* probs = new double[_maxKey+1];
//         for (int i = 1; i <= _maxKey; i++) {
//             probs[i] = (((double)1) / pow((double) i, _alpha)) / c;
//         }

//         // Random should be seeded already (in main)
//         std::random_shuffle(probs + 1, probs + maxKey);

//         sum_probs = new double[_maxKey+1];
//         sum_probs[0] = 0;
//         for (int i = 1; i <= _maxKey; i++) {
//             sum_probs[i] = sum_probs[i - 1] + probs[i];
//         }
        
//         delete[] probs;
//     }
//     ~KeyGeneratorZipfData() {
//         delete[] sum_probs;
//     }
// };
// template <typename K>
// class KeyGeneratorZipf {
// private:
//     PAD;
//     KeyGeneratorZipfData * data;
//     RandomFNV1A * rng;
//     PAD;
// public:
//     KeyGeneratorZipf(KeyGeneratorZipfData * _data, RandomFNV1A * _rng)
//           : data(_data), rng(_rng) {}
//     K next() {
//         double z; // Uniform random number (0 < z < 1)
//         int zipf_value = 0; // Computed exponential value to be returned

//         // Pull a uniform random number (0 < z < 1)
//         do {
//             z = (rng->next() / (double) std::numeric_limits<uint64_t>::max());
//         } while ((z == 0) || (z == 1));

//         zipf_value = std::upper_bound(data->sum_probs + 1, data->sum_probs + data->maxKey + 1, z) - data->sum_probs;

//         // Assert that zipf_value is between 1 and N
//         assert((zipf_value >= 1) && (zipf_value <= data->maxKey));

//         return (zipf_value);
//     }
// };

// template <typename K>
// class KeyGeneratorZipf {
// private:
//     PAD;
//     KeyGeneratorZipfData * dist;
//     RandomFNV1A * rng;
//     const int size = 10000000;
//     PAD;
//     K* data;
//     int ix;
//     PAD;
// public:
//     KeyGeneratorZipf(KeyGeneratorZipfData * _dist, RandomFNV1A * _rng)
//           : dist(_dist), rng(_rng), data(new K[size]), ix(0) {
//         for (int i = 0; i < size; ++i) {
//             // if (!(i % 100000)) cout << i << endl;

//             double z; // Uniform random number (0 < z < 1)
//             int zipf_value = 0; // Computed exponential value to be returned

//             // Pull a uniform random number (0 < z < 1)
//             do {
//                 z = (rng->next() / (double) std::numeric_limits<uint64_t>::max());
//             } while ((z == 0) || (z == 1));

//             zipf_value = std::upper_bound(dist->sum_probs + 1, dist->sum_probs + dist->maxKey + 1, z) - dist->sum_probs;

//             // Assert that zipf_value is between 1 and N
//             assert((zipf_value >= 1) && (zipf_value <= dist->maxKey));

//             data[i] = zipf_value;
//         }
//     }
//     K next() {
//         assert(ix < size);
//         return data[(ix++) % size];
//     }
// };


// Sampler taken from https://commons.apache.org/proper/commons-math/apidocs/src-html/org/apache/commons/math4/distribution/ZipfDistribution.html#line.44
// Paper: Rejection-Inversion to Generate Variates from Monotone Discrete Distributions.
struct ZipfRejectionInversionSamplerData {
    int* mapping;
    const int maxkey;
    ZipfRejectionInversionSamplerData(int _maxkey): maxkey(_maxkey) {
        mapping = new int[maxkey + 1];
        #pragma omp parallel for
        for (int i = 0; i < maxkey + 1; ++i) {
            mapping[i] = i;
        }
        std::random_shuffle(mapping + 1, mapping + maxkey);
    }

    ~ZipfRejectionInversionSamplerData() {
        delete[] mapping;
    }
};


class ZipfRejectionInversionSampler {
    const double exponent;
    const int maxkey;
    RandomFNV1A* rng;
    ZipfRejectionInversionSamplerData* const data;
    double hIntegralX1;
    double hIntegralmaxkey;
    double s;

    double hIntegral(const double x) {
        return helper2((1 - exponent) * log(x)) * log(x);
    }

    double h(const double x) {
        return exp(-exponent * log(x));
    }

    double hIntegralInverse(const double x) {
        double t = x * (1 - exponent);
        if (t < -1) {
            // Limit value to the range [-1, +inf).
            // t could be smaller than -1 in some rare cases due to numerical errors.
            t = -1;
        }
        return exp(helper1(t) * x);
    }

    double helper1(const double x) {
        // if (abs(x)>1e-8) {
            return log(x + 1)/x;
        // }
        // else {
        //     return 1.-x*((1./2.)-x*((1./3.)-x*(1./4.)));
        // }
    }

    double helper2(const double x) {
        // if (FastMath.abs(x)>1e-8) {
            return (exp(x) - 1)/x;
        // }
        // else {
        //     return 1.+x*(1./2.)*(1.+x*(1./3.)*(1.+x*(1./4.)));
        // }
    }

public:
    /** Simple constructor.
     * @param maxkey number of elements
     * @param exponent exponent parameter of the distribution
     */
    ZipfRejectionInversionSampler(ZipfRejectionInversionSamplerData* const _data, const double _exponent, RandomFNV1A * _rng): data(_data), maxkey(_data->maxkey), exponent(_exponent), rng(_rng) {
        if (exponent <= 1) {
            cout << "Zipf exponent must be greater than 1" << endl;
            exit(-1);
        }
        hIntegralX1 = hIntegral(1.5) - 1;
        hIntegralmaxkey = hIntegral(maxkey + 0.5);
        s = 2 - hIntegralInverse(hIntegral(2.5) - h(2));
    }

    /** Generate one integral number in the range [1, maxkey].
     * @param random random generator to use
     * @return generated integral number in the range [1, maxkey]
     */
    int next() {
        while(true) {
            // Pull a uniform random number (0 < z < 1)
            const double z = (rng->next() / (double) std::numeric_limits<uint64_t>::max());
            const double u = hIntegralmaxkey + z * (hIntegralX1 - hIntegralmaxkey);
            // u is uniformly distributed in (hIntegralX1, hIntegralmaxkey]

            double x = hIntegralInverse(u);

            int k = (int)(x + 0.5);

            if (k < 1) {
                k = 1;
            }
            else if (k > maxkey) {
                k = maxkey;
            }

            if (k - x <= s || u >= hIntegral(k + 0.5) - h(k)) {
                return data->mapping[k];
            }
        }
    }

};

#endif /* KEYGEN_H */

